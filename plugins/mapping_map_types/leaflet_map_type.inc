<?php

/**
 * Mapping Map Type plugin for the mapping_example
 * module.  Provides a basic map type for Google maps.
 */

/**
 * Define the Ctools plugin options.
 */
$plugin = array(
  'title' => t('Leaflet Map Type'),
  'description' => t(''),
  'map_type' => array(
    'class' => 'leaflet_map_type',
    'parent' => 'mapping_map_type',
  ),
);

/**
 * Google Map Class
 *
 * This provides a map for the example.
 */
class leaflet_map_type extends mapping_map_type {

  var $map_handler = 'leaflet_map_type';

  function __construct($export = array(), $map = array()) {
    parent::__construct($export, $map);
    $this->data = $this->merge_options($this->data, $this->options_default());
  }
  
  function options_default() {
    return array(
      'center' => array(0, 0),
      'sensor' => FALSE,
    );
  }

  function options_form() {
    return array(
      'center' => array(
        '#type' => 'textfield',
        '#title' => t('Map center'),
        '#description' => t('The default center of the map.'),
        '#options' => array(),
        '#default_value' => $this->data['center'],
      ),
    );
  }

  function render() {
    drupal_add_library('leaflet', 'leaflet');
    drupal_add_js(drupal_get_path('module', 'leaflet') . '/plugins/mapping_map_types/leaflet_map_type.js');
  }
}
